package com.test.banregio.sql;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.test.banregio.models.Account;
import com.test.banregio.models.Loan;


import java.util.ArrayList;

public class SQL extends SQLiteOpenHelper {
    private static final String TAG = "SQL_test";
    SQLiteDatabase db;
    public Activity activity;

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "banregio_test.db";

    private static final String CREATE_LOAN_TABLE = "CREATE TABLE IF NOT EXISTS "
            + "loan("
            + "id INTEGER NOT NULL, "
            + "customer TEXT NOT NULL, "
            +  "loan_date TEXT NOT NULL, "
            +  "amount NUMBER NOT NULL,"
            +  "status TEXT NOT NULL" +
            ");";

    private static final String CREATE_CUSTOMER_TABLE = "CREATE TABLE IF NOT EXISTS "
            + "account("
            + "customer TEXT NOT NULL, "
            + "amount NUMBER NOT NULL, "
            + "status TEXT NOT NULL"
            + ");";


    public SQL(Activity activity) {
        super(activity, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();
        db.execSQL(CREATE_LOAN_TABLE);
        db.execSQL(CREATE_CUSTOMER_TABLE);
    }

    /*public Service insertService(int serviceId, String json, boolean hasBegun){
        Service service;
        int begun = hasBegun?1:0;
        ContentValues values = new ContentValues();
        values.put(SERVICE_ID, serviceId);
        values.put(HAS_BEGUN, begun);
        values.put(JSON, json);
        service = getServiceById(serviceId);
        if (service.getId() == 0) {
            db = getWritableDatabase();
            long id = db.insert(TABLE_SERVICE, null, values);
            if (id>0){
                service = getServiceById(serviceId);
            }
        } else {
            service = updateService(serviceId, json, hasBegun);
        }
        db.close();
        return service;
    }*/

    /*public Service updateService(int serviceId, String json, boolean hasBegun) {
        int begun = hasBegun?1:0;
        ContentValues values = new ContentValues();
        values.put(HAS_BEGUN, begun);
        values.put(JSON, json);
        db = getWritableDatabase();
        boolean data = db.update(TABLE_SERVICE, values, "service_id=?", new String[]{String.valueOf(serviceId)}) > 0;
        if (data){
            return getServiceById(serviceId);
        }else{
            return new Service();
        }
    }*/

    public void fillTables() {
        db = getWritableDatabase();
        db.delete("loan", null, null);
        db.delete("account", null, null);
        ContentValues values = new ContentValues();
        values.put("id",1);
        values.put("customer", "00103228");
        values.put("loan_date", "2021-01-10");
        values.put("amount", 37500);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",2);
        values.put("customer", "00103228");
        values.put("loan_date", "2021-01-19");
        values.put("amount", 725.18);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",3);
        values.put("customer", "00103228");
        values.put("loan_date", "2021-01-31");
        values.put("amount", 1578.22);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",4);
        values.put("customer", "00103228");
        values.put("loan_date", "2021-02-04");
        values.put("amount", 380.00);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",1);
        values.put("customer", "70099925");
        values.put("loan_date", "2021-01-07");
        values.put("amount", 2175.25);
        values.put("status", Loan.PAID);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",2);
        values.put("customer", "70099925");
        values.put("loan_date", "2021-01-13");
        values.put("amount", 499.99);
        values.put("status", Loan.PAID);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",3);
        values.put("customer", "70099925");
        values.put("loan_date", "2021-01-24");
        values.put("amount", 5725.18);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",4);
        values.put("customer", "70099925");
        values.put("loan_date", "2021-02-07");
        values.put("amount", 876.13);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",1);
        values.put("customer", "00298185");
        values.put("loan_date", "2021-02-04");
        values.put("amount", 545.55);
        values.put("status", Loan.PENDING);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("id",1);
        values.put("customer", "15000125");
        values.put("loan_date", "2020-12-31");
        values.put("amount", 15220);
        values.put("status", Loan.PAID);
        db.insert("loan", null, values);

        values = new ContentValues();
        values.put("customer", "00103228");
        values.put("amount", 15375.28);
        values.put("status", Account.ACTIVE);
        db.insert("account", null, values);

        values = new ContentValues();
        values.put("customer", "70099925");
        values.put("amount", 3728.51);
        values.put("status", Account.LOCK);
        db.insert("account", null, values);

        values = new ContentValues();
        values.put("customer", "00298185");
        values.put("amount", 0.00);
        values.put("status", Account.CANCEL);
        db.insert("account", null, values);

        values = new ContentValues();
        values.put("customer", "15000125");
        values.put("amount", 235.28);
        values.put("status", Account.ACTIVE);
        db.insert("account", null, values);

        db.close();


    }



    public ArrayList<Loan> getLoansByCustomer(String customerNumber, String status) {
        ArrayList<Loan> loans = new ArrayList<>();
        db = getReadableDatabase();
        String rawSql = "SELECT id, customer, loan_date, amount, status FROM loan WHERE" +
                " customer like '" + customerNumber + "' AND status like '"+ status + "' ORDER BY loan_date ASC;";
        Log.i("_test", rawSql);
        Cursor cursor = db.rawQuery(rawSql, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                loans.add(new Loan(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        cursor.getDouble(3),
                        cursor.getString(4)
                ));
            }
        }
        db.close();
        return loans;
    }

    public ArrayList<Account> getAccounts(String status){
        ArrayList<Account> accounts = new ArrayList<>();
        db = getReadableDatabase();
        String rawSql = "SELECT customer, amount, status FROM account WHERE status like '" + status +"';";
        Cursor cursor = db.rawQuery(rawSql, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                accounts.add(new Account(
                        cursor.getString(0),
                        cursor.getDouble(1),
                        cursor.getString(2)
                ));
            }
        }
        db.close();
        return accounts;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
