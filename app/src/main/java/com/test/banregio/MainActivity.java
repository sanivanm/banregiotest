package com.test.banregio;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.test.banregio.adapters.AccountAdapter;
import com.test.banregio.adapters.PaymentsAdapter;
import com.test.banregio.models.Account;
import com.test.banregio.models.Loan;
import com.test.banregio.models.Payment;
import com.test.banregio.sql.SQL;
import com.test.banregio.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static RecyclerView paymentList;
    private static RecyclerView accountList;
    private Button btnChangeParameters;
    private static MainActivity activity;
    private String defaultDate = "2021-02-14";
    private double defaultInterestRate = 7.5;
    private double defaultTaxRate = 16.0;
    private int defaultDays = 360;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.hide();
        MainActivity.activity = this;
        btnChangeParameters = findViewById(R.id.btn_change_parameters);
        btnChangeParameters.setOnClickListener(this);


        paymentList = findViewById(R.id.payment_list);
        accountList = findViewById(R.id.account_list);
        calcultate(defaultDate, defaultInterestRate, defaultTaxRate, defaultDays);
    }

    public static void calcultate(String date, double interestRate, double taxRate, int days) {
        AccountAdapter accountAdapter;
        PaymentsAdapter adapter;
        SQL sql = new SQL(activity);
        sql.fillTables();
        ArrayList<Account> accounts = sql.getAccounts(Account.ACTIVE);
        ArrayList<Account> accountsToShow = new ArrayList<>();
        ArrayList<Payment> payments = new ArrayList<>();
        ArrayList<Loan> loansToShow = new ArrayList<>();
        for (int i = 0; i < accounts.size(); i++) {
            boolean iHaveToAdded = false;
            Account account = accounts.get(i);
            ArrayList<Loan> loans = sql.getLoansByCustomer(account.getCustomer(), Loan.PENDING);
            for (int j = 0; j < loans.size(); j++) {
                Loan loan = loans.get(j);
                if (account.getAmount() - loan.getAmount() > 0 ) {
                    Payment payment = new Payment();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date firstDate = null;
                    Date secondDate = null;
                    try {
                        firstDate = sdf.parse(date);
                        secondDate = sdf.parse(loan.getLoanDate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
                    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

                    payment.setTerm(String.valueOf(diff));
                    payment.setCustomer(account.getCustomer());
                    payment.setAmount(loan.getAmount());
                    double interest = loan.getAmount() * diff * (interestRate / 100) / days;
                    payment.setInterest(interest);
                    double iva = interest * (taxRate / 100);
                    payment.setIva(iva);
                    double paymentAmount = loan.getAmount() + interest + iva;
                    payment.setPayment(paymentAmount);
                    payments.add(payment);
                    accounts.get(i).setAmount(account.getAmount() - paymentAmount);
                    account = accounts.get(i);
                    loan.setStatus(Loan.PAID);
                    iHaveToAdded = true;
                }else{
                    Log.i("_test", "totalAmount:" + loan.getAmount()+" new amount: " + account.getAmount());
                }
            }
            if (iHaveToAdded){
                accountsToShow.add(account);
            }
        }

        adapter = new PaymentsAdapter(activity);
        adapter.setPayments(payments);
        paymentList.setAdapter(adapter);
        paymentList.setLayoutManager(new LinearLayoutManager(activity));
        adapter.notifyDataSetChanged();

        accountAdapter = new AccountAdapter(activity);
        accountAdapter.setAccounts(accountsToShow);
        accountList.setAdapter(accountAdapter);
        accountList.setLayoutManager(new LinearLayoutManager(activity));
        accountAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_change_parameters:
                Utils.ChangeParametersDialog dialog = new Utils.ChangeParametersDialog(this);
                dialog.setCancelable(false);
                dialog.show();
                break;
        }
    }
}