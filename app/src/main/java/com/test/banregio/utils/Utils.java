package com.test.banregio.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.test.banregio.MainActivity;
import com.test.banregio.R;
import com.test.banregio.models.Payment;

import org.json.JSONArray;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Utils {


    public static String numberFormat(double number, int decimals) {
        StringBuilder decimalsText = new StringBuilder((decimals > 0) ? "." : "");
        for (int i = 0; i < decimals; i++) {
            decimalsText.append("0");
        }
        DecimalFormat formateador = new DecimalFormat("###,###,###,##0"+decimalsText);
        return formateador.format(number);
    }

    public static String getText(EditText editText) {
        return editText.getText().toString().trim();
    }

    public static class ChangeParametersDialog extends Dialog implements View.OnClickListener {
        private String dateSelected = "2021-02-14";
        private TextView tvDate;
        private Activity activity;
        private Button btnChange;
        private EditText etTaxRate, etInterestRate, etDays;

        public ChangeParametersDialog(Activity activity) {
            super(activity);
            this.activity = activity;
        }

        public String getDateSelected() {
            return dateSelected;
        }

        public void setDateSelected(String dateSelected) {
            this.dateSelected = dateSelected;
        }

        private void openCalendar(){
            final Calendar c = Calendar.getInstance();
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            c.add(Calendar.YEAR, -1);
            int year = c.get(Calendar.YEAR);
            @SuppressLint("SetTextI18n") DatePickerDialog dialog = new DatePickerDialog(activity, (view, year1, month1, dayOfMonth) -> {
                String dayString = (dayOfMonth<10)?"0"+dayOfMonth:""+dayOfMonth;
                String monthDate = (month1+1<10)?"0"+(month1 + 1):""+(month1+1);
                dateSelected = year1 + "-" + monthDate + "-" + dayString;
                tvDate.setText(dayString + "/" + monthDate + "/" + year1);
            }, year, month, day);
            dialog.setCancelable(false);
            dialog.getDatePicker().setMinDate(System.currentTimeMillis());
            dialog.show();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_parameters);
            tvDate = findViewById(R.id.tv_date);
            tvDate.setOnClickListener(this);

            btnChange = findViewById(R.id.btn_change);
            btnChange.setOnClickListener(this);

            etTaxRate = findViewById(R.id.et_tax_rate);
            etInterestRate = findViewById(R.id.et_interest_rate);
            etDays = findViewById(R.id.et_business_year_days);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tv_date:
                    openCalendar();
                    break;
                case R.id.btn_change:
                    double interestRate = Double.parseDouble(Utils.getText(etInterestRate));
                    double taxRate = Double.parseDouble(Utils.getText(etTaxRate));
                    int days = Integer.parseInt(Utils.getText(etDays));
                    MainActivity.calcultate(dateSelected, interestRate, taxRate, days);
                    dismiss();
                    break;
            }
        }

    }
}
