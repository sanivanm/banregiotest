package com.test.banregio.models;

public class Loan {
    private int id;
    private String customer;
    private String loanDate;
    private double amount;
    private String status;

    public static final String PENDING = "Pendiente";
    public static final String PAID = "Pagado";

    public Loan(int id, String customer, String loanDate, double amount, String status) {
        this.id = id;
        this.customer = customer;
        this.loanDate = loanDate;
        this.amount = amount;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getLoanDate() {
        return loanDate;
    }

    public void setLoanDate(String loanDate) {
        this.loanDate = loanDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
