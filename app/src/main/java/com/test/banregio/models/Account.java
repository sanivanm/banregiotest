package com.test.banregio.models;

public class Account {
    private String customer;
    private double amount;
    private String status;

    public static final String ACTIVE = "Activa";
    public static final String LOCK = "Bloqueada";
    public static final String CANCEL = "Cancelada";

    public Account(String customer, double amount, String status) {
        this.customer = customer;
        this.amount = amount;
        this.status = status;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
