package com.test.banregio.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.test.banregio.R;
import com.test.banregio.models.Account;
import com.test.banregio.models.Payment;
import com.test.banregio.utils.Utils;

import java.util.ArrayList;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.AccountHolder> {
    private ArrayList<Account> accounts = new ArrayList<>();
    private Activity activity;


    public AccountAdapter(Activity activity) {
        this.activity = activity;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public AccountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_account, parent, false);
        return new AccountHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountHolder holder, int position) {
        Account account = accounts.get(position);
        holder.tvAmount.setText(getActivity().getString(R.string.money_symbol) + " " + Utils.numberFormat(account.getAmount(), 2));
        holder.tvCustomer.setText(account.getCustomer());
    }

    @Override
    public int getItemCount() {
        return accounts.size();
    }

    public class AccountHolder extends RecyclerView.ViewHolder {
        TextView tvAmount, tvCustomer;
        public AccountHolder(@NonNull View view) {
            super(view);
            tvAmount = view.findViewById(R.id.tv_amount);
            tvCustomer = view.findViewById(R.id.tv_customer);
        }
    }
}
