package com.test.banregio.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.test.banregio.R;
import com.test.banregio.models.Payment;
import com.test.banregio.utils.Utils;

import java.util.ArrayList;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.PaymentHolder> {
    private ArrayList<Payment> payments = new ArrayList<>();
    private Activity activity;

    public PaymentsAdapter(Activity activity) {
        this.activity = activity;
    }

    public ArrayList<Payment> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<Payment> payments) {
        this.payments = payments;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @NonNull
    @Override
    public PaymentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payment, parent, false);
        return new PaymentHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull PaymentHolder holder, int position) {
        Payment payment = payments.get(position);
        holder.tvCustomer.setText(payment.getCustomer());
        holder.tvTerm.setText(payment.getTerm());
        holder.tvAmount.setText(getActivity().getString(R.string.money_symbol) + " " + Utils.numberFormat(payment.getAmount(), 2));
        holder.tvInteres.setText(getActivity().getString(R.string.money_symbol) + " " + Utils.numberFormat(payment.getInterest(), 2));
        holder.tvIVA.setText(getActivity().getString(R.string.money_symbol) + " " + Utils.numberFormat(payment.getIva(), 2));
        holder.tvPayment.setText(getActivity().getString(R.string.money_symbol) + " " + Utils.numberFormat(payment.getPayment(), 2));
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }

    public class PaymentHolder extends RecyclerView.ViewHolder {
        TextView tvCustomer, tvTerm, tvAmount, tvInteres, tvIVA, tvPayment;

        public PaymentHolder(@NonNull View view) {
            super(view);
            tvCustomer = view.findViewById(R.id.tv_customer);
            tvTerm = view.findViewById(R.id.tv_term);
            tvAmount = view.findViewById(R.id.tv_amount);
            tvInteres = view.findViewById(R.id.tv_interest);
            tvIVA = view.findViewById(R.id.tv_tax);
            tvPayment = view.findViewById(R.id.tv_payment);
        }
    }
}
